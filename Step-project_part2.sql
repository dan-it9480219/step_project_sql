-- 1. Створіть базу даних для управління курсами. База має включати наступні таблиці:
-- - students: student_no, teacher_no, course_no, student_name, email, birth_date.
-- - teachers: teacher_no, teacher_name, phone_no
-- - courses: course_no, course_name, start_date, end_date
DROP DATABASE IF EXISTS school;

CREATE DATABASE school;

USE school;

CREATE TABLE IF NOT EXISTS courses(
course_no INT AUTO_INCREMENT PRIMARY KEY,
course_name VARCHAR(100),
start_date DATE,
end_date DATE);

CREATE TABLE IF NOT EXISTS teachers(
teacher_no INT AUTO_INCREMENT PRIMARY KEY,
teacher_name VARCHAR(30),
phone_no VARCHAR(20));

CREATE TABLE IF NOT EXISTS students(
student_no INT AUTO_INCREMENT PRIMARY KEY,
teacher_no INT,
FOREIGN KEY (teacher_no) REFERENCES teachers(teacher_no),
course_no INT,
FOREIGN KEY (course_no) REFERENCES courses(course_no),
student_name VARCHAR(30),
email VARCHAR(320),
birth_date DATE);

-- 2. Додайте будь-які данні (7-10 рядків) в кожну таблицю.
START TRANSACTION;

INSERT INTO school.courses(course_name,start_date,end_date)
VALUES
('Python Developer','2021-11-23','2022-05-03'),
('Java Developer','2022-02-11','2022-08-05'),
('Frontend Developer','2022-05-01','2022-10-01'),
('.NET Developer','2023-01-11', '2023-10-01'),
('Data Engineer','2023-01-15','2023-08-02'),
('Data Scientist','2023-03-15','2023-09-12'),
('Machine Learning','2023-12-01','2024-05-02'),
('Data Analyst','2024-01-23','2024-06-03'),
('QA','2024-09-12','2024-12-12');

SELECT * FROM courses;

INSERT INTO school.teachers(teacher_name,phone_no)
VALUES
('Casen Barton','077 1331 8333'),
('Frank Compton','079 7234 7517'),
('Julie Bush','078 6478 1490'),
('Zahir Jacobson','079 8220 8777'),
('Celia Sanford','070 1796 0146 '),
('Truett Barker','078 7711 0188'),
('Ryland Quintero','077 3315 0035'),
('Andres Bartlett','079 2370 7063');

SELECT * FROM teachers;

INSERT INTO school.students(teacher_no,course_no,student_name,email,birth_date)
VALUES
(2,5,'Lennox Roman', 'mailarc@outlook.com', '2001-01-07'),
(1,2,'Kian Gilmore','fallorn@me.com','1997-10-11'),
(4,3,'Keyla Patel','pkilab@optonline.net','1993-07-21'),
(5,8,'Reyna Stark','formis@aol.com','1983-11-30'),
(1,2,'Parker Morrow','gastown@outlook.com','1989-05-12'),
(5,8,'Louisa Doyle','euice@gmail.com','1975-08-17'),
(7,9,'Chanel Ellison','engelen@att.net','2004-03-24'),
(8,1,'Kye George', 'granboul@mac.com','2006-12-21'),
(4,3,'Adelyn Moody','tangsh@me.com','1991-02-14'),
(3,6,'Kristopher Hor','hillct@yahoo.com','1993-01-29'),
(1,2,'Avah Collins','frode@live.com','2001-09-21'),
(3,6,'Miles Rosario','amimojo@icloud.com','1995-04-11'),
(6,4,'Kashton Green','gavollink@outlook.com','1993-07-21'),
(5,8,'Zoe Beasley','north@optonline.net','1993-07-21'),
(3,7,'Stanley Montoya','munjal@sbcglobal.net','1998-06-01'),
(2,5,'Mark Finley','fukuchi@optonline.net','1993-09-23'),
(7,9,'Nala Olson','shrapnull@att.net','1997-06-11'),
(1,2,'Malachi Felix','arachne@outlook.com','1998-09-24'),
(2,5,'Paisleigh Perkins','muadip@gmail.com','1987-07-18'),
(8,1,'Kyrie Harding','jaxweb@sbcglobal.net','2001-12-31'),
(7,9,'Aniya Porter','north@hotmail.com','2004-01-05'),
(6,4,'Rhett Hartman','aegreene@yahoo.com','2003-05-14'),
(3,7,'Johnathan Foster','nasor@att.net','2004-02-04');

SELECT * FROM students;

COMMIT;   

-- 3. По кожному викладачу покажіть кількість студентів, з якими він працював
SELECT st.teacher_no, st.teacher_name, COUNT(ss.student_no) AS student_number
FROM school.teachers AS st
JOIN school.students AS ss ON st.teacher_no=ss.teacher_no
GROUP BY st.teacher_no, st.teacher_name
ORDER BY st.teacher_no;

-- 4. Спеціально зробіть 3 дубляжі в таблиці students (додайте ще 3 однакові рядки) 
START TRANSACTION;

INSERT INTO school.students(teacher_no,course_no,student_name,email,birth_date)
VALUES
(1,2,'Avah Collins','frode@live.com','2001-09-21'),
(5,8,'Zoe Beasley','north@optonline.net','1993-07-21'),
(1,2,'Malachi Felix','arachne@outlook.com','1998-09-24');

-- 5. Напишіть запит який виведе дублюючі рядки в таблиці students
SELECT student_name, email, COUNT(email)
FROM school.students 
GROUP BY student_name, email
HAVING COUNT(email) > 1;

ROLLBACK;


