-- 1. Покажіть середню зарплату співробітників за кожен рік, до 2005 року.
SELECT YEAR(es.from_date) AS report_year, ROUND(AVG(es.salary),0) AS avg_salary
FROM employees AS ee
JOIN salaries AS es ON ee.emp_no=es.emp_no
GROUP BY YEAR(es.from_date)
HAVING report_year BETWEEN MIN(YEAR(es.from_date)) AND 2005
ORDER BY report_year;

-- 2. Покажіть середню зарплату співробітників по кожному відділу. 
-- Примітка: потрібно розрахувати по поточній зарплаті, та поточному відділу співробітників
SELECT de.dept_no, ROUND(AVG(es.salary),0) AS avg_dept_salary
FROM employees.salaries AS es
JOIN employees.dept_emp AS de ON es.emp_no=de.emp_no 
WHERE es.to_date>CURDATE() AND de.to_date>CURDATE()
GROUP BY de.dept_no
ORDER BY de.dept_no;

-- 3. Покажіть середню зарплату співробітників по кожному відділу за кожний рік
SELECT YEAR(es.from_date) AS report_year, 
	   de.dept_no, 
       ROUND(AVG(es.salary),0) AS avg_dept_salary
FROM employees.salaries AS es
JOIN employees.dept_emp AS de ON es.emp_no=de.emp_no 
JOIN employees.employees AS ee ON es.emp_no=ee.emp_no 
GROUP BY YEAR(es.from_date), de.dept_no
ORDER BY report_year;

-- 4. Покажіть відділи, в яких зараз працює більше 15000 співробітників.
SELECT de.dept_no, COUNT(ee.emp_no) AS num_emp
FROM employees.employees AS ee
JOIN employees.dept_emp AS de ON ee.emp_no=de.emp_no
WHERE de.to_date>CURDATE()
GROUP BY de.dept_no
HAVING num_emp > 15000;

-- 5. Для менеджера який працює найдовше покажіть його номер, відділ, дату прийому на роботу, прізвище
SELECT dm.emp_no, dm.dept_no, ee.hire_date, ee.last_name
FROM employees.dept_manager AS dm
JOIN employees.employees AS ee ON dm.emp_no=ee.emp_no AND dm.to_date>CURDATE()
WHERE TIMESTAMPDIFF(YEAR, ee.hire_date, CURDATE()) = (SELECT MAX(TIMESTAMPDIFF(YEAR, ee.hire_date, CURDATE()))
													 FROM dept_manager AS dm
                                                     JOIN employees AS ee ON dm.emp_no=ee.emp_no);

-- 6. Покажіть топ-10 діючих співробітників компанії з найбільшою різницею між їх зарплатою і 
-- середньою зарплатою в їх відділі.
WITH 
	avg_dept_salary AS (SELECT d.dept_no, 
							   ROUND(AVG(es.salary),0) AS avg_dept_sal
						FROM dept_emp AS de
						JOIN employees.departments AS d ON de.dept_no=d.dept_no
						JOIN employees.salaries AS es ON de.emp_no=es.emp_no AND es.to_date>CURDATE()
						GROUP BY d.dept_no), -- середня зп кожного відділу
    emp_dept_sal_dif AS (SELECT ads.dept_no, 
								de1.emp_no,
								es1.salary,
								ads.avg_dept_sal,
								ROUND(ABS(AVG(es1.salary)-ads.avg_dept_sal),0) AS salary_dif
						 FROM avg_dept_salary AS ads
                         JOIN dept_emp AS de1 ON ads.dept_no=de1.dept_no AND de1.to_date>CURDATE()
                         JOIN employees.salaries AS es1 ON de1.emp_no=es1.emp_no AND es1.to_date>CURDATE()
                         GROUP BY de1.emp_no, ads.dept_no, es1.salary
                         ORDER BY salary_dif DESC
                         LIMIT 10)
SELECT ee.emp_no, 
	   edsd.dept_no, 
       CONCAT(ee.first_name, ' ', ee.last_name) AS employee, 
       edsd.salary,
	   edsd.avg_dept_sal, 
       edsd.salary_dif
FROM emp_dept_sal_dif AS edsd
JOIN employees.employees AS ee ON edsd.emp_no=ee.emp_no;

-- 7. Для кожного відділу покажіть другого по порядку менеджера. Необхідно вивести відділ, 
-- прізвище ім’я менеджера, дату прийому на роботу менеджера і дату коли він став менеджером відділу
SELECT man_in.dept_no, 
       CONCAT(ee.first_name, ' ', ee.last_name) AS manager_name,
       ee.hire_date,
       man_in.from_date
FROM (SELECT *, ROW_NUMBER() OVER (PARTITION BY dept_no ORDER BY from_date) AS row_num
	  FROM employees.dept_manager) AS man_in
JOIN employees.employees AS ee ON man_in.emp_no=ee.emp_no
WHERE row_num = 2;

